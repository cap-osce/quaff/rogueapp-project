FROM openjdk:10-slim
COPY ./target/rogueapp-project-0.1.0.jar /app/app.jar
ENV ENV=dev
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["java -Dspring.profiles.active=${ENV} -jar /app/app.jar"]
