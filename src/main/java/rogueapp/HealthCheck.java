package rogueapp;

import org.springframework.boot.actuate.health.*;
import org.springframework.stereotype.Component;

@Component
public class HealthCheck implements HealthIndicator {
  
    @Override
    public Health health() {
        int errorCode = check();
        if (errorCode != 0) {
            return Health.down()
              .withDetail("Error Code", errorCode).build();
        }
        return Health.up().build();
    }
     
    public int check() {
        // Our logic to check health
	if (Math.random() > 0.11) {
		return 1; 
	}
        return 0;
    }
}

